# Lembretes de comandos do shell do GNU/Linux

## 1 - Ações básicas com arquivos

- [Versão em PDF](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/tabela-acoes-basicas-com-arquivos-v3.pdf)

### Listar diretórios e arquivos

| Comando | Descrição |
|---|---|
| `ls [OPÇÕES] [CAMINHOS]` | Sintaxe geral |
| `ls` | Lista arquivos e diretórios no caminho atual. |
| `ls -a` | Inclui arquivos e diretórios ocultos na listagem. |
| `ls -l` | Exibe listagem no formato detalhado. |
| `ls -la` | Inclui arquivos e diretórios ocultos na listagem detalhada. |
| `echo *` | O mesmo que ls, mas mantém nomes de arquivos com espaços e outros símbolos especiais do shell como uma palavra só. |

### Em que diretório você está?

| Comando | Descrição |
|---|---|
| `pwd` | Exibe o caminho até o diretório atual. |

### Mudar de diretório

| Comando | Descrição |
|---|---|
| `cd [CAMINHO]` | Muda para o diretório expresso em `CAMINHO`. |
| `cd` ou `cd ~` | Muda para o diretório pessoal do utilizador (sua ***home***). |
| `cd -` | Muda para o último diretório visitado. |

### Criar um diretório

| Comando | Descrição |
|---|---|
| `mkdir CAMINHOS` | Cria os diretórios expressos em `CAMINHOS`. |
| `mkdir -p CAMINHOS` | Cria os novos diretórios omitindo erros caso eles já existam ou se os diretórios dos `CAMINHOS` até eles não existirem: neste último caso, também cria os demais diretórios dos `CAMINHOS`. |

### Criar arquivos vazios

| Comando | Descrição |
|---|---|
| `touch NOMES` | Cria os arquivos `NOMES` no diretório corrente. |
| `>> NOME` ou `:>> NOME` | O mesmo que `touch`, mas não altera a *timestamp* do arquivo. |

### Criar ligações simbólicas

| Comando | Descrição |
|---|---|
| `ln -s CAMINHO LINK` | Cria uma ligação simbólica no caminho do `LINK` para o arquivo ou diretório em `CAMINHO`. |

### Remover arquivos e diretórios (cuidado!)

| Comando | Descrição |
|---|---|
| `rm ARQUIVOS` | Remove os arquivos especificados. |
| `rm -r DIRETÓRIOS` | Remove os diretórios especificados e seus respectivos conteúdos. |

### Copiar arquivos e diretórios

| Comando | Descrição |
|---|---|
| `cp ARQ1 ARQ2` | Cria uma cópia de `ARQ1` em `ARQ2`. |
| `cp -r DIR1 DIR2` | Cria uma cópia de `DIR1` em `DIR2`. |

### Mover ou renomear arquivos e diretórios

| Comando | Descrição |
|---|---|
| `mv CAMINHO1 CAMINHO2` | Altera o nome do arquivo ou diretório em `CAMINHO1` para `CAMINHO2`. |

### Encontrar arquivos e diretórios
| Comando | Descrição |
| --- |--- |
| find `CAMINHO -name ARQUIVO` | Pesquisa `ARQUIVO` a partir do `CAMINHO` especificado. |
| Exemplos: | |
| `find /etc -name sources.list` | Pesquisa o arquivo ou diretório `sources.list` no diretório `/etc`. |
| `find $HOME -name -type d documents` | Pesquisa um diretório com o nome `documents` a partir do diretório pessoal do usuário. |
| `find . -type f -iname *.ogg` | Pesquisa exclusivamente arquivos que terminem com a extensão `ogg`, considerando maiúsculas ou minúsculas. |  

### Alterar permissões de arquivos

| Comando | Descrição |
|---|---|
| `chmod ±x ARQUIVO` | Ativa (`+`) ou desativa (`-`) o atributo de **execução** de `ARQUIVO`. |
| `chmod ±r ARQUIVO` | Ativa (`+`) ou desativa (`-`) o atributo de **leitura** de `ARQUIVO`. |
| `chmod ±w ARQUIVO` | Ativa (`+`) ou desativa (`-`) o atributo de **escrita** em `ARQUIVO`. |
| `g`±atributo | Limita a alteração do atributo apenas ao **grupo**. |
| `u`±atributo | Limita a alteração do atributo apenas ao **usuário**. |
| `o`±atributo | Aplica a alteração do atributo a todos os outros, menos ao **usuário**. |
| `a`±atributo | Aplica a alteração do atributo a *todos* (padrão por omissão). |
| Exemplo: `chmod u+rw,g+r,o-rwx ARQUIVO` | Permite que o dono leia e escreva, o grupo apenas leia e os outros não tenham acesso algum. | 

### Alterar posse de arquivos e diretórios
| Comando | Descrição |
| --- | --- |
| `chown dono:grupo ARQUIVO` | atribui um novo dono e grupo ao `ARQUIVO`. |

### Exibir o conteúdo de arquivos de texto

| Comando | Descrição |
|---|---|
| `cat ARQUIVO` | Imprime o conteúdo de `ARQUIVO` no terminal. |
| `grep PALAVRA ARQUIVO` | Pesquisa `PALAVRA` em `ARQUIVO` |
| `wc ARQUIVO` | Exibe a quantidade de linhas, palavras e caracteres em `ARQUIVO` |
| `tac ARQUIVO` | Imprime o conteúdo de `ARQUIVO` no terminal, mas com as linhas na ordem inversa. |
| `head ARQUIVO` | Imprime as 10 primeiras linha de `ARQUIVO` no terminal. |
| `tail ARQUIVO` | Imprime as 10 últimas linha de `ARQUIVO` no terminal. |
| `less ARQUIVO`<br/>`more ARQUIVO` | Exibem o conteúdo de `ARQUIVO` em uma saída rolável (*paginadores*). |

### Enviar linhas de texto para arquivos

| Comando | Descrição |
|---|---|
| `echo TEXTO > ARQUIVO` <br/>`COMANDO > ARQUIVO` | Trunca (apaga) o conteúdo de `ARQUIVO` e escreve nele a saída de `COMANDO`. |
| `echo TEXTO >> ARQUIVO`<br/>`COMANDO >> ARQUIVO` | Adiciona a saída de `COMANDO` ao final do conteúdo de `ARQUIVO`. |

### Enviar linhas de texto para comandos

| Comando | Descrição |
|---|---|
| `echo TEXTO \| COMANDO`<br/>`COMANDO1 \| COMANDO2` | Redireciona a saída de um `COMANDO` para a entrada de outro `COMANDO` (***pipe***). |

### Editar arquivos de texto plano

| Comando | Descrição |
|---|---|
| `nano ARQUIVO` | Editor simples e completo. |
| `vim ARQUIVO`  | Editor modal com capacidades avançadas. |

